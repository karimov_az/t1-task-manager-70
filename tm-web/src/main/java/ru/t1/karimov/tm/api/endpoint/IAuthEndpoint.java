package ru.t1.karimov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.t1.karimov.tm.model.Result;
import ru.t1.karimov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("api/auth")
public interface IAuthEndpoint {

    @Nullable
    @WebMethod
    @GetMapping("/profile")
    User profile();

    @NotNull
    @WebMethod
    @GetMapping(value = "/login", produces = "application/json")
    Result login(@RequestParam("username") @WebParam(name = "username") String username,
                 @RequestParam("password") @WebParam(name = "password") String password);

    @NotNull
    @WebMethod
    @GetMapping(value = "/logout", produces = "application/json")
    Result logout();

}
