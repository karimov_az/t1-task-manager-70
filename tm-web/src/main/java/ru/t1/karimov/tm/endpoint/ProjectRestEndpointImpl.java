package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.karimov.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.service.ProjectService;
import ru.t1.karimov.tm.util.UserUtil;

import javax.jws.WebParam;
import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpointImpl implements IProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody @Nullable final Project project
    ) {
        projectService.removeByUserId(project, UserUtil.getUserId());
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void delete(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @Nullable final String id
    ) {
        projectService.removeByIdAndUserId(id, UserUtil.getUserId());
    }

    @NotNull
    @Override
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectService.findAllByUserId(UserUtil.getUserId());
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @Nullable final String id
    ) {
        return projectService.findOneByIdAndUserId(id, UserUtil.getUserId());
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public Project save(
            @WebParam(name = "project", partName = "project")
            @RequestBody @Nullable final Project project
    ) {
        projectService.addByUserId(project, UserUtil.getUserId());
        return project;
    }

}
