package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.karimov.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.service.TaskService;
import ru.t1.karimov.tm.util.UserUtil;

import javax.jws.WebParam;
import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpointImpl implements ITaskRestEndpoint {

    @NotNull
    @Autowired
    private TaskService taskService;

    @Override
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody @Nullable final Task task
    ) {
        taskService.removeByUserId(task, UserUtil.getUserId());
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void delete(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @Nullable final String id
    ) {
        taskService.removeByIdAndUserId(id, UserUtil.getUserId());
    }

    @NotNull
    @Override
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return taskService.findAllByUserId(UserUtil.getUserId());
    }

    @Override
    @Nullable
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return taskService.findOneByIdAndUserId(id, UserUtil.getUserId());
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public Task save(
            @WebParam(name = "task", partName = "task")
            @RequestBody @Nullable final Task task
    ) {
        taskService.addByUserId(task, UserUtil.getUserId());
        return task;
    }

}
