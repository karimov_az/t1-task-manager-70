package ru.t1.karimov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Table(name = "tm_role")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Role {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @ManyToOne
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    public Role(@NotNull final RoleType roleType) {
        this.roleType = roleType;
    }

    @NotNull
    @Override
    public String toString() {
        return roleType.name();
    }

}
