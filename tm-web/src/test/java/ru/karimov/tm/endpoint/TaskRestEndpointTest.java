package ru.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.karimov.tm.marker.IntegrationCategory;
import ru.t1.karimov.tm.client.ITaskEndpointClient;
import ru.t1.karimov.tm.model.Task;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Category(IntegrationCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TaskRestEndpointTest {

    @NotNull
    static final String BASE_URL = "http://localhost:8080/api/tasks";

    @NotNull
    private final ITaskEndpointClient taskEndpointClient = ITaskEndpointClient.taskClient(BASE_URL);

    @NotNull
    private final Task taskTest1 = new Task("Task Test 1");

    @NotNull
    private final Task taskTest2 = new Task("Task Test 2");

    @NotNull
    private final Task taskTest3 = new Task("Task Test 3");

    @NotNull
    private Collection<Task> tasks = new ArrayList<>();

    @Before
    public void init() {
        taskEndpointClient.save(taskTest1);
        taskEndpointClient.save(taskTest2);
        taskEndpointClient.save(taskTest3);
        tasks = taskEndpointClient.findAll();
    }

    @After
    public void clean() {
        taskEndpointClient.delete(taskTest1);
        taskEndpointClient.delete(taskTest2);
        taskEndpointClient.delete(taskTest3);
        tasks.clear();
    }

    @Test
    public void delete() {
        for (@NotNull final Task task : tasks) {
            taskEndpointClient.delete(task);
        }
        @NotNull final Collection<Task> actualTasks = taskEndpointClient.findAll();
        assertEquals(0, actualTasks.size());
    }

    @Test
    public void deleteById() {
        for (@NotNull final Task task : tasks) {
            @NotNull final String id = task.getId();
            taskEndpointClient.delete(id);
        }
        @NotNull final Collection<Task> actualTasks = taskEndpointClient.findAll();
        assertEquals(0, actualTasks.size());
    }

    @Test
    public void findAll() {
        @NotNull final Collection<Task> actualTask = taskEndpointClient.findAll();
        assertEquals(tasks.size(), actualTask.size());
    }

    @Test
    public void findById() {
        for (@NotNull final Task task : tasks) {
            @NotNull final String expectedId = task.getId();
            @Nullable final Task actualTask = taskEndpointClient.findById(expectedId);
            assertNotNull(actualTask);
            assertEquals(expectedId, actualTask.getId());
        }
    }

    @Test
    public void save() {
        @NotNull final Task taskTest4 = new Task("Task Test 4");
        taskEndpointClient.save(taskTest4);
        @NotNull final String expectedId = taskTest4.getId();
        @NotNull final String expectedName = taskTest4.getName();
        @Nullable final Task actualTask = taskEndpointClient.findById(expectedId);
        assertNotNull(actualTask);
        assertEquals(expectedName, actualTask.getName());

        taskEndpointClient.delete(taskTest4);
    }

}
