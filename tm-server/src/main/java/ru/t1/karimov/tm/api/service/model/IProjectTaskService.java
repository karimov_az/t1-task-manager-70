package ru.t1.karimov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.model.Task;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    void removeProjectById(
            @Nullable String userId,
            @Nullable String projectId
    );

    void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

}
