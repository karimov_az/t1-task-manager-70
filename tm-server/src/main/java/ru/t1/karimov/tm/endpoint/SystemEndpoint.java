package ru.t1.karimov.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import ru.t1.karimov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.karimov.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.karimov.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.karimov.tm.dto.response.system.ApplicationVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.karimov.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ApplicationAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final ApplicationAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ApplicationVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST) @NotNull final ApplicationVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
